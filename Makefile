# PRESENTATION_FILES = AnalFuncional.tex GeneracionPerfil.tex ValidacionPerfil.tex AppDiseno.tex LeyMov.tex AppImplementacion.tex Requerimientos.tex
PRESENTATION_FILES = $(shell ls Sections/*.tex)
PRESENTATION_DIR = Sections
TARGET = presentation.tex

# DEP = $(patsubst %, $(PRESENTATION_DIR)/%,$(PRESENTATION_FILES))
COMPILER = pdflatex
FLAGS= -draftmode

all: presentation.pdf

presentation.pdf: $(PRESENTATION_FILES)
	$(COMPILER) $(TARGET)
	$(COMPILER) $(TARGET)

print:
	echo $(PRESENTATION_FILES)

compile:
	$(COMPILER) $(TARGET)

draft:
	$(COMPILER) $(FLAGS) $(TARGET)

clean:
	rm $(PRESENTATION_DIR)/*.aux *.aux *.log *.out *.toc *.snm *.nav $(PRESENTATION_DIR)/*.log $(PRESENTATION_DIR)/*~ *~

.PHONY: all compile clean draft
